#!/bin/bash
# Packages typically installed on a new Arch (or Gentoo) installation
set -e
set -o pipefail

usage()
{
    echo "usage : install [ -d DISTRO ]"
}

while getopts "d:?h" option
do
    case $option in
	d)
	    DISTRO=${OPTARG};;
	h|?)
            usage
            exit 0;;
    esac
done


function print_stage() {
    echo -e " \e[33m*\e[39m \e[94m$1\e[39m"
}

# System administration (mostly required for AUR, base-devel pulls in many things including sudo, gcc etc.)
#AUDIO=(mpd gmpc pulseaudio pavucontrol alsa-utils snapcast ymuse xfce4-mpc-plugin xfmpc)
AUDIO=(mpd pulseaudio pavucontrol alsa-utils pipewire snapcast xfce4-mpc-plugin xfmpc)
BROWSERS=(firefox lynx nyxt vivaldi browserpass-firefox browserpass-chromium)
COMPRESSION=(bzip2 gzip xz)
DESKTOP=(xorg-server xf86-video-intel xf86-input-libinput xf86-input-synaptics lightdm lightdm-gtk-greeter xfce4
         xfce4-goodies thunar lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings)
DEVELOPMENT=(act-bin difftastic emacs mermaid-cli r parallel shellcheck)
FONTS=(ttf-font-awesome-5 ttf-material-design-icons ttf-weather-icons ttf-octicons ttf-atom-file-icons ttf-all-the-icons
       ttf-hack)
GRAPHICS=(darktable gimp hugin ristretto)
LATEX=(pandoc texlab texlive-most texlive-bibtexextra texlive-fontsextra)
MISC=(bat fd lsd duf lshw most tldr which xclip xdotool)
NETWORKING=(iwd networkmanager network-manager-applet networkmanager-openvpn networkmanager-vpnc dhclient dnscrypt-proxy)
OFFICE=(libreoffice evince)
PYTHON=(python python-virtualenvs python-virtualenvwrapper python-pre-commit python-lsp-server python-lsp-ruff
        python-lsp-black ruff uv python-setuptools python-pip)
R=(R)
SECURITY=(age keychain pass xclip xdotool)
SHELL=(kitty tmux zsh)
SYSTOOLS=(cups etckeeper fcron gcc tree keychain lshw logrotate rsync git wget gcc-fortran htop)
VERSION_CONTROL=(direnv git-absorb jj yadm)
VIDEO=(ffmpeg mpv vlc)
YUBIKEY=(age-plugin-yubikey yubico-pam yubikey-personalization yubikey-manager)
WEB_SERVERS=(caddy nginx)

if [ -z "${DISTRO}" ]; then
  print_stage "No DISTRO specified checking /etc/os-release"
  DISTRO=$(grep "^ID=" /etc/os-release | sed 's/ID=//')
  print_stage "Looks like you're using ${DISTRO} Linux."
fi


if [ "${DISTRO}" == "arch" ]; then
  print_stage "Adding packages specific to ${DISTRO}"
  ARCH=(base-devel usbutils pacman-contrib)
  pacman -Syu "${ARCH[@]}" \
       "${SECURITY[@]}" \
       "${DESKTOP[@]}" \
       "${NETWORKING[@]}" \
       "${SYSTOOLS[@]}" \
       "${GRAPHICS[@]}" \
       "${AUDIO[@]}" \
       "${VIDEO[@]}" \
       "${BROWSERS[@]}" \
       "${DEVELOPMENT[@]}" \
       "${OFFICE[@]}" \
       "${LATEX[@]}" \
       "${PYTHON[@]}" \
       "${SHELL[@]}" \
       "${MISC[@]}" \
       "${VERSION_CONTROL[@]}" \
       "${FONTS[@]}"
elif [ "${DISTRO}" == "gentoo" ]; then
  print_stage "Adding packages specific to ${DISTRO}"
  GENTOO=(eix genkernel)
  print_stage "Syncing portage"
  emerge --sync
  emerge --uDU "${ARCH[@]}" \
       "${SECURITY[@]}" \
       "${DESKTOP[@]}" \
       "${NETWORKING[@]}" \
       "${SYSTOOLS[@]}" \
       "${GRAPHICS[@]}" \
       "${AUDIO[@]}" \
       "${VIDEO[@]}" \
       "${BROWSERS[@]}" \
       "${DEVELOPMENT[@]}" \
       "${OFFICE[@]}" \
       "${LATEX[@]}" \
       "${PYTHON[@]}" \
       "${SHELL[@]}" \
       "${MISC[@]}" \
       "${VERSION_CONTROL[@]}" \
       "${FONTS[@]}"
else
  print_stage "Sorry the ${DISTRO} is not yet supported by this script."
fi
