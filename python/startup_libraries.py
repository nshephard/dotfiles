"""
Libraries to load on starting ipython
"""
from datetime import datetime
from pathlib import Path
import re
print
import numpy as np
import pandas as pd
import matplotlib.plotly as plt
