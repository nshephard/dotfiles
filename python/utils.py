"""Useful functions."""
from decimal import Decimal
from pathlib import Path
from typing import Union


def find_files(base_dir: Union[str, Path] = None, file_ext: str = ".spm") -> List:
    """Scan the specified directory for files with the given file extension.

    Parameters
    ----------
    base_dir: Union[str, Path]
        Directory to recursively search for files, if not specified the current directory is scanned.
    file_ext: str
        File extension to search for.

    Returns
    -------
    List
        List of files found with the extension in the given directory.
    """
    base_dir = Path("./") if base_dir is None else Path(base_dir)
    return list(base_dir.glob("**/*" + file_ext))


def convert_path(path: Union[str, Path]) -> Path:
    """Ensure path is Path object.

    Parameters
    ----------
    path: Union[str, Path]
        Path to be converted.

    Returns
    -------
    Path
        pathlib Path
    """
    return Path().cwd() if path == "./" else Path(path)


def get_key_for_value(dictionary: dict, value: Union[str, int, float]) -> Union[list, str, int, float]:
    """Extract the key from a dictionary for a given value.

    Parameters
    ----------
    dictionary: dict
        Dictionary to be searched.
    value: Union[str, int, float]
        Value to be searched for.

    Returns
    -------
    Union[list, str,int,float]
        A The key associated with the value.
    """
    matching_keys = []
    for key, val in dictionary.items():
        if val == value:
            matching_keys.append(key)
    if len(matching_keys) == 0:
        return None
    elif len(matching_keys) == 1:
        return matching_keys[0]
    return matching_keys


def dict_to_decimal(dictionary: dict) -> dict:
    """Recursively convert any floats in a dictionary to Decimal.

    Parameters
    ----------
    dictionary: dict
        Dictionary to be converted.

    Returns
    -------
    dict
        Dictionary with floats converted to Decimal
    """
    for key, value in dictionary.items():
        if isinstance(value, dict):
            dict_to_decimal(value)
        else:
            try:
                dictionary[key] = Decimal(value)
            except:
                pass
    return dictionary


def update_config(config: dict, args: Union[dict, Namespace]) -> Dict:
    """Update a dictionary configuration with any arguments.

    Parameters
    ----------
    config: dict
        Dictionary of configuration (typically read from YAML file specified with '-c/--config <filename>')
    args: Namespace
        Command line arguments
    Returns
    -------
    Dict
        Dictionary updated with command arguments.
    """
    args = vars(args) if isinstance(args, Namespace) else args
    # FIXME : Needs this the other way round so it works with recursion
    config_keys = config.keys()
    for arg_key, arg_value in args.items():
        if arg_key in config_keys and arg_value is not None:
            original_value = config[arg_key]
            config[arg_key] = arg_value
            LOGGER.info(f"Updated config config[{arg_key}] : {original_value} > {arg_value} ")
    config["base_dir"] = convert_path(config["base_dir"])
    config["output_dir"] = convert_path(config["output_dir"])
    return config
