#### Filename              .Rprofile
### Author                nshephard@gmail.com
### Created               2012/01/26
### Description           Sets up my preferred profile
#.First <- function(){
#}
## Set CRAN mirror
options(repos=c(RStudio='https://cran.rstudio.com/', getOption('repos')))
#local({r <- getOption("repos"); r["CRAN"] <- "https://cran.uk.r-project.org"; options(repos=r)})
## Set the locale
#Sys.setlocale(category = "LC_ALL", "en_GB.UTF-8")

## Default NOT to import strings as factors as they're often done wrong
## and we want to do them in our chosen manner
options(stringsAsFactors = FALSE,
        useNA            = "ifany",
        xtable.sanitize.text.function=identity,
        browser          = "firefox")

## Set styler and linter optiosn
options(styler.save_after_styling = TRUE,
        styler.addins_style_transformer = styler::tidyverse_style())


## Save the history but not the data image
## utils::assignInNamespace(
##     "q",
##     function(save = "no", status = 0, runLast = TRUE){
##         ## Save the history...
##         ## ToDo - Currently an issue with ESS see https://github.com/emacs-ess/ESS/issues/291
##         ## savehistory(file = '.Rhistory')
##         .Internal(quit(save, status, runLast))
##     },
##     "base"
## )


if(interactive()){
    options(warn = -1, quietly = TRUE)
    # suppressMessages(library(tidyverse, quietly = TRUE))
    suppressMessages(library(devtools, quietly = TRUE))
    suppressMessages(library(fcuk, quietly = TRUE))
    suppressMessages(library(foreign, quietly = TRUE))
    suppressMessages(library(googledrive, quietly = TRUE))
    suppressMessages(library(googlesheets4, quietly = TRUE))
    suppressMessages(library(Hmisc, quietly = TRUE))
    suppressMessages(library(knitr, quietly = TRUE))
    suppressMessages(library(kableExtra, quietly = TRUE))
    suppressMessages(library(magrittr, quietly = TRUE))
    ## suppressMessages(library(nmisc, quietly = TRUE))
    suppressMessages(library(reticulate, quietly = TRUE))
    suppressMessages(library(rmarkdown, quietly = TRUE))
    suppressMessages(library(RSQLite, quietly = TRUE))
    ## suppressMessages(library(shiny, quietly = TRUE))
    suppressMessages(library(spelling, quietly = TRUE))
    cat("\n",
        base::date(),
        "\n\n",
        rmsfact::rmsfact(),
        "\n\n",
        "\t\t\t\t - Richard M. Stallman",
        "\n\n")
    print(fortunes::fortune())
}

## Some simple functions that serve as aliases for changing between
## commonly used directories
host <- system("uname -n", intern = TRUE)

## APIs
api <- list()

## GoogleSheets/Gargle (see https://gargle.r-lib.org/articles/non-interactive-auth.html)
## to get a JSON with token.
## gmail = "nshephard@gmail.com"
## googledrive::drive_auth(email=gmail)
## googlesheets4 <- gs4_auth(
##     email=gmail,
##     scopes = "https://www.googleapis.com/auth/drive.readonly",
## )

## Git branch details (https://twitter.com/ndiquattro/status/1293986045290192897)
if(requireNamespace("prompt", quietly=TRUE)){
    prompt_git <- function(...){
        paste0("[", prompt::git_branch(), "]", " > ")
    }
    prompt::set_prompt(prompt_git)
    rm(prompt_git)
}

## Simple UNIX like functions
pwd <- function() {
    getwd()
}
cd <- function(path) {
    setwd(path)
}
l <- function() {
    system("ls -lha")
}
print0 <- function(...) {
    print(paste0(...))
}

## httpgd (https://github.com/nx10/httpgd)
##
## See also https://www.youtube.com/watch?v=uxyhmhRVOfw
##
## Start a device for displaying graphs and tables
# httpgd::hgd()
view <- function(.data){
  if(interactive()) {
    reactable::reactable(.data,
              filterable = TRUE,
              searchable = TRUE,
              showPageSizeOptions = TRUE,
              striped = TRUE,
              highlight = TRUE,
              compact = TRUE,
              defaultPageSize = 30)
  }
}

## Helper functions
##
## Set names on a data frame (https://stackoverflow.com/a/69807168/1444043)
set_names <- function(df, column_names) {
    stopifnot("Column names must be character." = class(column_names) != "character")
    stopifnot("df must be a data.frame" = ! "data.frame" %in% class(df))
    stopifnot("Number of column names must match number of columns" = length(names(df)) != length(column_names))
    names(df) <- column_names
}
