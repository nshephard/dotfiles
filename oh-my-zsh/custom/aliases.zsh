# Redefine some common commands to use aliases
alias ls='lsd'
alias l='ls -lha'
alias lla='ls -la'
alias lt='ls --tree'
alias rm='rm -I'
alias du1='du -h --max-depth=1 | sort -h'
alias grep='grep --colour=auto'
alias egrep='egrep --colour=auto'
alias fgrep='fgrep --colour=auto'
alias diff='colordiff'
alias tree='tree -fhD'
alias bc='bc -l'
alias ip='ip -c'
alias youtube-dl='yt-dlp --thumbnail --embed-thumbnail'

# rsync aliases
alias rsync='rsync -a --info=progress2'
alias rsync_html='rsync ~/org/*.html ~/org/export ~/org/training ovh:~/www/. --exclude="*.org"'

## grep history
ghist() {
    history | grep "$@"
}

# Emacs
alias emacsc='/usr/bin/emacsclient -nc --socket-name=${EMACS_SOCKET} &'
alias emacsn='/usr/bin/emacsclient -nw --socket-name=${EMACS_SOCKET}'

## Awesome Command Line Tools https://github.com/chubin/awesome-console-services
## Weather https://github.com/chubin/wttr.in
weather () {
    curl wttr.in/"$@"
}
weather2 () {
    curl v2d.wttr.in/"$@"
}
alias weather_sheffield='curl wttr.in/Sheffield'
alias weather_high_neb='curl wttr.in/High+Neb'

## Linux commands https://github.com/chubin/cheat.sheets
cheat () {
    curl cheat.sh/"$@"
}

## Git
# Copmare current HEAD to HEAD~1 (i.e. last commit)
alias gdh='git diff HEAD~1 HEAD'


## Wake On Lan
alias wol_kimura='wol 38:d5:47:e0:16:1f'
alias wol_alarmpi_4b='wol dc:a6:32:c4:71:b9'

## Gentoo
alias genlopt='genlop -l --date today'
alias genlopw='genlop -l --date week'

## Arch
alias systemctl.emacs='systemctl --user restart emacs'

## Git
gpush() {
    CURRENT=$(pwd)
    cd ${HOME}/dotfiles
    print_stage "Pushing : $(pwd)"
    git push
    cd ${HOME}/.password-store
    print_stage "Pushing : $(pwd)"
    git push
    cd ${HOME}/.config/emacs
    print_stage "Pushing : $(pwd)"
    git push
    cd ${HOME}/org
    print_stage "Pushing : $(pwd)"
    git push
    cd ${HOME}/org-roam
    print_stage "Pushing : $(pwd)"
    git push
    cd ${HOME}/work/org
    print_stage "Pushing : $(pwd)"
    git push
    cd ${HOME}/work/ns-rse.github.io/
    print_stage "Pushing : $(pwd)"
    git push
    cd ${CURRENT}
}
gpull() {
    CURRENT=$(pwd)
    cd ${HOME}/dotfiles
    print_stage "Pulling : $(pwd)"
    git pull
    cd ${HOME}/.password-store
    print_stage "Pulling : $(pwd)"
    git pull
    cd ${HOME}/.config/emacs
    print_stage "Pulling : $(pwd)"
    git pull
    cd ${HOME}/org
    print_stage "Pulling : $(pwd)"
    git pull
    cd ${HOME}/org-roam
    print_stage "Pulling : $(pwd)"
    git pull
    cd ${HOME}/work/org
    print_stage "Pulling : $(pwd)"
    git pull
    cd ${HOME}/work/ns-rse.github.io/
    print_stage "Pulling : $(pwd)"
    git pull
    cd ${CURRENT}
}

## SSH alias'
alias slacker='ssh root@slacker'
alias darwin='ssh darwin'
alias fisher='ssh fisher'
alias hamilton='ssh hamilton'
alias kimura='ssh kimura'
alias kimura.local='ssh 192.168.1.45'
alias kimura.fisher='ssh -t kimura "ssh fisher"'
alias openwrt='ssh openwrt'
alias ovh='ssh ovh'
alias morgan='ssh morgan'
alias work='ssh work'
alias mendel='ssh mendel'
alias laptop='ssh laptop'
alias alarmpi='ssh alarmpi'
alias alarmpi_ext='ssh neil@alarmpi.net'
alias alarmpi2='ssh neil@alarmpi2'
alias alarmpi3='ssh neil@alarmpi3'
alias alarmpi4='ssh neil@alarmpi4'
alias alarmpi5='ssh neil@alarmpi5'
alias alarmpi6='ssh neil@alarmpi6'
alias alarmpi7='ssh neil@alarmpi7'
alias alarmpi-4b='ssh neil@alarmpi-4b'
alias jim='ssh nshephard@frankexchangeofviews.co.uk'

## tmux aliases
alias tmux_ssh='tmux attach -t ssh'

# Alias for commonly used directories (@ home)
alias projects='cd ~/work/projects/'
alias templates='cd ~/work/templates'
alias code_clinic='cd ~/work/code_clinic/'
alias emacsd='cd ~/.config/emacs/'
alias org='cd ~/org'
alias orgroam='cd ~/work/org-roam'
alias pics='cd ~/pics'


# Python
alias pl='pylint --rcfile=~/.pylintrc'
alias pt='pytest --pylint --mpl --cov-report=html'
alias pip_upgrade='pip list --outdated --format=freeze | grep -v "^\-e" | cut -d = -f 1  | xargs -n1 pip install -U'

# Create a new directory and enter it
mkcd() {
    mkdir -p "$@"
    cd "$_" # || exit
}

# Generate QR codes https://www.linuxjournal.com/content/boost-productivity-bash-tips-and-tricks
qrgen () {
    printf "$@" | curl -F-=\<- qrenco.de
}
