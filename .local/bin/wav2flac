#!/bin/bash
# Convert FLAC to MP3 reserving meta-data
# From https://stackoverflow.com/questions/26109837/convert-flac-to-mp3-with-ffmpeg-keeping-all-metadata
# ffmpeg -i input.flac -ab 320k -map_metadata 0 -id3v2_version 3 output.mp3

# Or there is this alternative from https://wiki.archlinux.org/index.php/Convert_Flac_to_Mp3
# parallel ffmpeg -i {} -qscale:a 0 {.}.mp3 ::: *.flac

## This searches the current path and converts  found flac to mp3 at 256kbps
# find -name "*.flac" -exec ffmpeg -i {} -acodec libmp3lame -ab 320k {}.mp3 \;
#
# Useful reading...
#
# https://unix.stackexchange.com/questions/9496/looping-through-files-with-spaces-in-the-names
#
# https://unix.stackexchange.com/questions/203531/gnu-parallel-quoted-arguments-and-spaces-in-file-names-how-to-solve
set -e
set -o pipefail

usage()
{
    echo "usage : flac2mp3 [ -d DIR ] [ -j JOBS ] [ -b BITRATE ]"
}

while getopts "d:j:b:?h" option
do
    case $option in
	d)
	    INPUT_DIR=${OPTARG};;
	j)
	    JOBS=${OPTARG};;
	b)
	    BITRATE=${OPTARG};;
	h|?)
            usage
            exit 0;;
    esac
done

if [ -z "${INPUT_DIR}" ]; then
    echo "You have not specified a directory path the current directory will be"
    echo "scanned for .flac files and any found will be converted to mp3."
    INPUT_DIR=$(pwd)
fi

if [ -z "${JOBS}" ]; then
    JOBS=8
fi

if [ -z "${BITRATE}" ]; then
    BITRATE=320k
fi

FILES=$(find "${INPUT_DIR}" -type f -name "*.wav")
#N_FILES=${FILES[@]}
echo "####################################################"
echo " Scanned for flac files in  : ${INPUT_DIR}"
# echo " Flac files found           : ${FILES[@]}"
echo " Parallel jobs              : ${JOBS}"
echo "####################################################"


_convert()
{
    echo "Converting file ${1}"
    ffmpeg -hide_banner -loglevel panic -i "${1}" -ab "${2}" -map_metadata 0 -id3v2_version 3 "${1}".flac
}
export -f _convert

echo ""
echo "*** Parallel execution using ${JOBS} CPU threads ***"
echo ""

# Set time to zero
SECONDS=0
# find ${INPUT_DIR} -type f -name "*.flac" | parallel -q -j ${JOBS} _convert {}
echo "${FILES}" | parallel -q -j ${JOBS} _convert {} $BITRATE
DURATION=${SECONDS}

echo "####################################################"
echo "Conversion took          : ${DURATION} seconds"
echo "####################################################"
