#!/bin/bash
# Search for files of a specified type and wrap them using fold to a user specified length
#
# Leverages parallel to do things a bit faster
#
# Useful reading...
#
# https://unix.stackexchange.com/questions/9496/looping-through-files-with-spaces-in-the-names
#
# https://unix.stackexchange.com/questions/203531/gnu-parallel-quoted-arguments-and-spaces-in-file-names-how-to-solve
set -e
set -o pipefail
set +o noclobber

usage()
{
    echo "usage : wrap [ -d DIR ] [ -j JOBS ] [ -l LENGTH ] [ -e EXT]"
}

while getopts "d:j:l:e:?h" option
do
    case $option in
	d)
	    INPUT_DIR=${OPTARG};;
	j)
	    JOBS=${OPTARG};;
	l)
	    LENGTH=${OPTARG};;
	e)
	    EXT=${OPTARG};;
	h|?)
            usage
            exit 0;;
    esac
done

function print_stage() {
    echo -e " \e[33m*\e[39m \e[94m$1\e[39m"
}

if [ -z "${EXT}" ]; then
    print_stage "You have not specified a file extension, defaulting to '.md'"
    INPUT_DIR=$(pwd)
fi

if [ -z "${LENGTH}" ]; then
    print_stage "You have not specified a line length, defaulting to 120"
    LENGTH=120
fi

if [ -z "${INPUT_DIR}" ]; then
    print_stage "You have not specified a directory path the current directory will be"
    print_stage "scanned."
    INPUT_DIR=$(pwd)
fi

if [ -z "${JOBS}" ]; then
    JOBS=8
fi


FILES=$(find "${INPUT_DIR}" -type f -name "*.${EXT}")
print_stage "####################################################"
print_stage " Scanned                    : ${INPUT_DIR}"
print_stage " *.${EXT} files found         : ${#FILES[@]}"
print_stage " Wrap length                : ${LENGTH}"
print_stage " Parallel jobs              : ${JOBS}"
print_stage "####################################################"

_convert()
{
    if [[ -f "${1}" ]]; then
        echo "Converting file ${1}"
        TMP=$(fmt -w "${2}" -s "${1}")
        echo "$TMP" > "${1}"
    else
        echo "${1} does not exist"
    fi
}
export -f _convert

print_stage ""
print_stage "*** Parallel execution using ${JOBS} CPU threads ***"
print_stage ""

# Set time to zero
SECONDS=0
echo "${FILES}" | parallel -q -j "${JOBS}" _convert {} "${LENGTH}"
DURATION=${SECONDS}

print_stage "####################################################"
print_stage " Conversion took          : ${DURATION} seconds"
print_stage "####################################################"
