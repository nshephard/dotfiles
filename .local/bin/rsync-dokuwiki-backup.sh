#!/bin/bash
#========================
# Script for backing up DokuWiki periodically, ideally add to crontab
# backup script for stuff on wiki to be run periodically through a crontab on a trusted server

# > crontab -e
#
# daily : 1 1 * * * /path/to/script/rsync-dokuwiki-backup.sh
# weekly: 1 1 * * 0 /path/to/script/rsync-dokuwiki-backup.sh
#
# Original : https://www.dokuwiki.org/tips:backup_script#an_rsync_alternative
#========================
set -e
set -o pipefail

function print_stage() {
    echo -e " \e[33m*\e[39m \e[94m$1\e[39m"
}

usage() {
    echo "usage: rsync-dokuwiki-backup.sh [ -s SITE ] [ -i IP_ADDRESS ][ -u REMOTE_USER ] [ -p PORT ] [ -r RELATIVE_PATH ] [ -e EXCLUDE_FILES_FROM ] [ -n N_BACKUP] [ -o OUTPUT_DIR ] [ -b BACKUP_NAME ] [ -a ARCHIVE_DAYS ]"
}

while getopts 's:i:u:p:r:e:n:o:b:a:?h' option
do
    case $option in
	a)
	    ARCHIVE_DAYS=${OPTARG};;
	b)
	    BACKUP_NAME=${OPTARG};;
	c)
	    SITE=${OPTARG};;
	e)
	    EXCLUDE_FILES_FROM=${OPTARG};;
	i)
	    IP_ADDRESS=${OPTARG};;
	n)
	    N_BACKUP=${OPTARG};;
	o)
	    OUTPUT_DIR=${OPTARG};;
	p)
	    PORT=${OPTARG};;
	r)
	    RELATIVE_PATH=${OPTARG};;
	s)
	    SITE=${OPTARG};;
	u)
	    REMOTE_USER=${OPTARG};;
	h|?)
            usage
	    exit 0;;
    esac
done


DIR="$(pwd)"
DATE="$(date '+%Y%m%d.%H%M')"

# Set parameters if no options specified
if [ -z "${IP_ADDRESS}" ]; then
    IP_ADDRESS="152.228.170.148"
    # print_stage "No IP Address specified, defaulting to : ${IP_ADDRESS}"
fi

if [ -z "${SITE}" ]; then
    SITE="kimura"
    # print_stage "No site specified, defaulting to : ${SITE}"
fi

if [ -z "${REMOTE_USER}" ]; then
    REMOTE_USER="arch"
    # print_stage "No user specified, defaulting to : ${REMOTE_USER}"
fi

if [ -z "${PORT}" ]; then
    PORT=562
    # print_stage "No port specified, defaulting to : ${PORT}"
fi

if [ -z "${RELATIVE_PATH}" ]; then
    if [ "${SITE}" == "sheffieldboulder" ]; then
        RELATIVE_PATH="/srv/http/sheffieldboulder"
    elif [ "${SITE}" == "neils-snaps" ]; then
        RELATIVE_PATH="/usr/share/webapps/wordpess"
    else
	RELATIVE_PATH="/srv/http/kimura"
    fi
    # print_stage "No relative path specified, setting based on ${SITE} : ${RELATIVE_PATH}"
fi

if [ -z "${EXCLUDE_FILES_FROM}" ]; then
    EXCLUDE_FILES_FROM="${DIR}/backup-exclude-list.txt"
    /usr/bin/touch ${EXCLUDE_FILES_FROM}
    # print_stage "No exclude list specified creating a blank"
fi

if [ -z "${N_BACKUP}" ]; then
    N_BACKUP=7
    # print_stage "No number of backups specified, defaulting to : ${N_BACKUP}"
fi

if [ -z "${OUTPUT_DIR}" ]; then
    OUTPUT_DIR="/home/neil/www/"
    # if [ "${SITE}" == "sheffieldboulder" ]; then
    #     OUTPUT_DIR="/home/neil/www/sheffieldboulder"
    # else
    # 	OUTPUT_DIR="/home/neil/www/kimura"
    # fi
    # print_stage "No output dir specified, defaulting to : ${OUTPUT_DIR}"
fi

if [ -z "${BACKUP_NAME}" ]; then
    BACKUP_NAME="backup"
    # print_stage "No backup name specified, defaulting to : ${BACKUP_NAME}"
fi

if [ -z "${ARCHIVE_DAYS}" ]; then
    ARCHIVE_DAYS=30
    # print_stage "No period for archiving specified, defaulting to : ${ARCHIVE_DAYS}"
fi



# Combine options
if [ ! -z "${IP_ADDRESS}" ]; then
    BACKUP_SRC="${REMOTE_USER}@${IP_ADDRESS}:${RELATIVE_PATH}"
else
    BACKUP_SRC="${REMOTE_USER}@${SERVER}:${RELATIVE_PATH}"
fi
BACKUP_DIR="${OUTPUT_DIR}/${SITE}"
LOGFILE="${BACKUP_DIR}/backup.log"
BACKUP_NAME="backup"

print_stage "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
print_stage "Performing backup...."
print_stage "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
print_stage "Host          : ${BACKUP_SRC}"
print_stage "Port          : ${PORT}"
print_stage "Remote User   : ${REMOTE_USER}"
print_stage "Destination   : ${BACKUP_DIR}"
print_stage "Exclude From  : ${EXCLUDE_FILES_FROM}"
print_stage "Backup Name   : ${BACKUP_NAME}"
print_stage "Backups Kept  : ${N_BACKUP}"
print_stage "Archive every : ${ARCHIVE_DAYS}"
print_stage "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
print_stage ""


# Check if a directory exists and if not create it
check_dir() {
    ARCHIVE_DIR=$1
    # print_stage "Checking if directory exists : ${ARCHIVE_DIR}"
    if [ ! -d "${ARCHIVE_DIR}" ] ; then
        /bin/mkdir -p "${ARCHIVE_DIR}"
    fi
}

# Function to rotate directories
rotate_dir() {
    PATH=$1
    NAME=$2
    N=$(($3 - 1))
    ARRAY=$(/usr/bin/seq ${N} -1 0)
    for i in $(/usr/bin/seq ${N} -1 0) ; do
	if [ -d "${PATH}/${NAME}-$i" ] ; then
	    # print_stage "Removing : ${PATH}/${NAME}-$((i + 1))"
            /bin/rm -rf "${PATH}/${NAME}-$((i + 1))"
	    # print_stage "Moving   : ${PATH}/$NAME-$i" "${PATH}/${NAME}-$((i + 1))"
            /bin/mv "${PATH}/$NAME-$i" "${PATH}/${NAME}-$((i + 1))"
	fi
    done
}

# Check and create archive and daily directories
check_dir "${BACKUP_DIR}/"
check_dir "${BACKUP_DIR}/archive"
check_dir "${BACKUP_DIR}/daily"
check_dir "${BACKUP_DIR}/daily/backup-0"

# Rotate backups
rotate_dir "${BACKUP_DIR}/daily" "$BACKUP_NAME" "$N_BACKUP"

# Move log-file
if [ -f ${LOGFILE} ] ; then
    print_stage "Moving log-file              : ${LOGFILE}"
    /bin/mv ${LOGFILE} ${BACKUP_DIR}/daily/${BACKUP_NAME}-1/.
fi


# Log-file header
/bin/cat >> ${LOGFILE} <<_EOF
=============================================================
  Backup done on: ${DATE}
=============================================================
  Host          : ${BACKUP_SRC}
  Port          : ${PORT}
  Destination   : ${BACKUP_DIR}
  Exclude From  : ${EXCLUDE_FILES_FROM}
  Backup Name   : ${BACKUP_NAME}
  Backups Kept  : ${N_BACKUP}
  Archive every : ${ARCHIVE_DAYS}
=============================================================

_EOF

print_stage ""
print_stage "Creating target directories..."
/bin/mkdir -p ${BACKUP_DIR}/daily/${BACKUP_NAME}-1/
/bin/mkdir -p ${BACKUP_DIR}/daily/${BACKUP_NAME}-0/
print_stage ""
print_stage "Changing to target directories..."
cd ${BACKUP_DIR}/daily/${BACKUP_NAME}-0
print_stage ""
print_stage "Performing backup, please be patient..."
/usr/bin/rsync -av -e "/usr/bin/ssh -p $PORT" --whole-file --delete --force \
    -b --backup-dir "${BACKUP_DIR}/daily/${BACKUP_NAME}-1/" \
    --exclude-from="${EXCLUDE_FILES_FROM}" \
    ${BACKUP_SRC} . \
    1>> ${LOGFILE} 2>&1
print_stage ""
print_stage "Backup complete."


LAST_ARCHIVE="0"
if [ -r ${BACKUP_DIR}/LAST_ARCHIVE ] ; then
  LAST_ARCHIVE=$(/bin/cat ${BACKUP_DIR}/LAST_ARCHIVE)
fi

print_stage ""
print_stage "Checking if archive needs to be created..."
NOW=$(/bin/date +%j)
print_stage "Last archive: ${LAST_ARCHIVE}"
print_stage "Today       : ${NOW}"
let DIFF_DAY=(${NOW} - ${LAST_ARCHIVE})
if [ "${DIFF_DAY}"  -ge "${ARCHIVE_DAYS}" ] || [ "${DIFF_DAY}" -lt 0 ]; then
    print_stage "It has been > ${ARCHIVE_DAYS} since last archive, creating archive..."
    print_stage "${NOW}" > ${BACKUP_DIR}/LAST_ARCHIVE
    cd ${BACKUP_DIR}/daily
    /bin/tar -cf ${BACKUP_DIR}/archive/${BACKUP_NAME}-"${DATE}".tar.xz ./${BACKUP_NAME}-0
    print_stage "Archive complete    : ${BACKUP_DIR}/archive/${BACKUP_NAME}-"${DATE}".tar.xz"

    # /bin/cat >> ${LOGFILE} <<_EOF
    # =============================================================
    #   Archive created at : ${BACKUP_DIR}/archive/${BACKUP_NAME}-"${DATE}".tar.xz
    # =============================================================
    # _EOF

fi
