# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

##############################################
# Set simple PS1 for when using TRAMP and exit
#
# https://emacs.stackexchange.com/a/48089
if [[ $TERM == "dumb" ]]; then
    unsetopt zle
    unsetopt prompt_cr
    unsetopt promppt_subst
    unfunction precmd
    unfunction preexec
    export PS1="$ "
    return
fi


# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Add to PATH
#path+=("/home/$USER/bin")
export PATH="/home/${USER}/bin:$PATH"
path+=("/home/$USER/.local/bin")
path+=("/home/$USER/.cargo/bin")
path+=("/home/$USER/.node/bin")


# Antigen
#source "$HOME"/.antigen/antigen.zsh
#antigen use oh-my-zsh

# Would you like to use another custom folder than $ZSH/custom?
#ZSH_CUSTOM="$HOME/.oh-my-zsh/custom"

# Completion
# https://git-scm.com/book/en/v2/Appendix-A%3A-Git-in-Other-Environments-Git-in-Zsh
autoload -Uz compinit promptinit
compinit
#promptinit; prompt gentoo
zstyle ':completion::complete:*' use-cache 1

# Emacs socket, this is modified subsequently if require on a per host basis.
if [ "${HOST}" = "kimura" ] || [ "${HOSTNAME}" = "kimura" ]; then
  export EMACS_SOCKET="/tmp/emacs1000/server"
else
  export EMACS_SOCKET="/run/user/1000/emacs/server"
fi
EMACSCLIENT=$(which emacsclient)
export EDITOR="${EMACSCLIENT} --socket-name=${EMACS_SOCKET}"

# ZSH theme, again modified on a per host basis if required. For alien the ALIEN_THEME is set on a per host basis
export ZSH_CUSTOM="${HOME}"/.oh-my-zsh/custom
# https://eendroroy.github.io/alien/
export ZSH_THEME="alien/alien"
export ALIEN_SECTION_USER_HOST=1
export ALIEN_SECTIONS_LEFT=(exit
                            time
                            user
                            path
                            vcs_branch:async
                            vcs_status:async
                            vcs_dirty:async
                            versions:async
                            newline
                            ssh
                            venv
                            prompt)
# export ALIEN_SECTIONS_RIGHT=(time)
# antigen theme eendroroy/alien alien

# Python & virtualenvwrapper
alias pip='noglob pip'
export PIP_REQUIRE_VIRTUALENV=true
export WORKON_HOME=${HOME}/.virtualenvs
export PROJECT_HOME=${HOME}/work/git/
if [ "${HOST}" = "crow" ] || [ "${HOSTNAME}" = "crow" ]; then
    source /run/current-system/sw/bin/virtualenvwrapper.sh
else
  source /usr/bin/virtualenvwrapper.sh
  export VIRTUALENVWRAPPER_PYTHON="/usr/bin/python"
fi

#source "$HOME"/.oh-my-zsh/custom/themes/alien/alien.zsh
# Set host specific parameters (ZSH theme, Emacs daemon socket)
if [ "${HOST}" = "kimura" ] || [ "${HOSTNAME}" = "kimura" ]; then
    export ALIEN_THEME="soft"
elif [ "${HOST}" = "mendel" ] || [ "${HOSTNAME}" = "mendel" ]; then
    export ALIEN_THEME="blue"
elif [ "${HOST}" = "alarmpi-4b" ] || [ "$HOSTNAME" = "alarmpi-4b" ]; then
    export EMACS_SOCKET="/run/user/1001/emacs/server"
    export ALIEN_THEME="green"
# elif [ "${HOST}" = "vps410177.ovh.net" ] || [ "${HOSTNAME}" = "vps410177.ovh.net" ]; then
elif [ "${HOST}" = "nshephard.dev" ] || [ "${HOSTNAME}" = "nshephard.dev" ]; then
    export ALIEN_THEME="red"
elif [ "${HOST}" = "fisher" ] || [ "${HOSTNAME}" = "fisher" ]; then
    export ALIEN_THEME="blue"
elif [ "${HOST}" = "haldane" ] || [ "${HOSTNAME}" = "haldane" ]; then
    export ALIEN_THEME="gruvbox"
elif [ "${HOST}" = "crow" ] || [ "${HOSTNAME}" = "crow" ]; then
    export ALIEN_THEME="blue"
    # export ZSH_THEME="powerlevel10k/powerlevel10k"
    export VIRTUALENVWRAPPER_PYTHON="/run/current-system/sw/bin/python"
fi

# Set ZSH_CUSTOM for themes to work
source "${ZSH_CUSTOM}/themes/${ZSH_THEME}.zsh"

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" "agnosterzak" "powerlevel10k/powerlevel10k" "bullet-train" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=7

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
setopt INC_APPEND_HISTORY
setopt EXTENDED_HISTORY
setopt HIST_FIND_NO_DUPS
export HISTTIMEFORMAT="[%F %T]"
export HIST_STAMPS="yyyy-mm-dd"
export SAVEHIST=1000000
export HISTSIZE=500000


# Enable compdef, zmv and others
autoload -U compinit && compinit
autoload zmv

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
    direnv
    git
    pass
    pip
    rsync
    common-aliases
    conda-zsh-completion
    F-Sy-H
    zsh-autopair
)


source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_GB.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='nano'
fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"


# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
source ~/.bash_aliases
if [ -f "~/.bash_aliases_tmp" ]; then
    source ~/.bash_aliases_tmp
fi
## Unset a few aliases
unalias duf
# unalias fd

# Was loosing ssh keys in remote tmux
# sessions so git actions required passwds
# https://github.com/tmux/tmux/wiki/FAQ#how-do-i-use-ssh-agent1-with-tmux
[ ! -f ~/.ssh.agent ] && ssh-agent -s >~/.ssh.agent
eval `cat ~/.ssh.agent` >/dev/null
if ! kill -0 "$SSH_AGENT_PID" 2>/dev/null; then
        ssh-agent -s >~/.ssh.agent
        eval `cat ~/.ssh.agent` >/dev/null
fi
##############################################
## https://superuser.com/a/424588           ##
##############################################
# if [ -S "$SSH_AUTH_SOCK" ] && [ ! -h "$SSH_AUTH_SOCK" ]; then
#     ln -sf "$SSH_AUTH_SOCK" ~/.ssh/ssh_auth_sock
# fi
# export SSH_AUTH_SOCK=~/.ssh/ssh_auth_sock

##############################################
## Keychain                                 ##
##############################################
export SSH_KEY_PATH="~/.ssh/"
export GPG_TTY=$(tty)
export KEYCHAIN=$(which keychain)
if [ "${HOST}" = "kimura" ] ||
     [ "${HOST}" = "haldane" ] ||
     [ "${HOST}" = "mendel" ] ||
     [ "${HOST}" = "crow" ] ||
     [ "${HOSTNAME}" = "kimura" ] ||
     [ "${HOSTNAME}" = "haldane" ] ||
     [ "${HOSTNAME}" = "mendel" ] ||
     [ "${HOSTNAME}" = "crow" ]  ; then
    # export SSH_AUTH_SOCK='$(gpgconf --list-dirs agent-ssh-socket)'
    ${KEYCHAIN} --agents ssh ~/.ssh/id_ed25519
    ${KEYCHAIN} --agents ssh ~/.ssh/haldane_ed25519
    if [ "${HOST}" = "crow" ]  ; then
        ${KEYCHAIN} --agents ssh ~/.ssh/crow_ed25519
    fi
    . ~/.keychain/"${HOST}"-sh
    ${KEYCHAIN} --agents gpg nshephard@protonmail.com
    ${KEYCHAIN} --agents gpg n.shephard@sheffield.ac.uk
    . ~/.keychain/"${HOST}"-sh-gpg
    if [[ -n "$SSH_CONNECTION" ]] ;then
        # export PINENTRY_USER_DATA="USE_CURSES=1"
        export PINENTRY_USER_DATA="USE_TTY=1"
    fi
else
    ${KEYCHAIN} --agents ssh ~/.ssh/id_ed25519
fi
# Seems to sort out OSX
if [ "${HOST}" = "Neils-MBP.lan" ] ; then
    ssh-add --apple-use-keychain ~/.ssh/id_ed25519
fi
# If we've SSH'd in connect/start a tmux session
if [[ -z "$TMUX" ]] && [ "$SSH_TTY" != "" ]; then
    tmux attach-session -t ssh_tmux || tmux new-session -s ssh_tmux
fi

# vterm options https://github.com/akermu/emacs-libvterm ##
vterm_printf(){
   if [ -n "${TMUX}" ]; then
        # Tell tmux to pass the escape sequences through
        # (Source: http://permalink.gmane.org/gmane.comp.terminal-emulators.tmux.user/1324)
        printf "\ePtmux;\e\e]%s\007\e\\" "$1"
    elif [ "${TERM%%-*}" = "screen" ]; then
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$1"
    else
        printf "\e]%s\e\\" "$1"
    fi
}
if [[ "${INSIDE_EMACS}" = 'vterm' ]]; then
    alias clear='vterm_printf "51;Evterm-clear-scrollback";tput clear'
fi
vterm_prompt_end() {
    vterm_printf "51;A$(whoami)@$(hostname):$(pwd)";
}
setopt PROMPT_SUBST
PROMPT=${PROMPT}'%{$(vterm_prompt_end)%}'

# MPD essentials
export MPD_HOST=192.168.1.28
export MPD_PORT=6600

# Kitty (https://sw.kovidgoyal.net/kitty/#zsh)
if [ ! "${TERMINFO}" = "/snap/emacs/current/usr/share/emacs/27.1/etc/" ]; then
    kitty + complete setup zsh | source /dev/stdin
fi

# most https://www.jedsoft.org/most/
if command -v most > /dev/null 2>&1; then
  export PAGER="most"
fi

# atuin https://docs.atuin.sh/
# eval "$(atuin init zsh)"

# Show the weather
curl v2d.wttr.in/Sheffield &

# >>> conda initialize >>>
# To activate this environment, use:

#     micromamba activate /home/neil/miniforge3

# Or to execute a single command in this environment, use:

#     micromamba run -p /home/neil/miniforge3 mycommand

# installation finished.
# Do you wish to update your shell profile to automatically initialize conda?
# This will activate conda on startup and change the command prompt when activated.
# If you'd prefer that conda's base environment not be activated on startup,
#    run the following command when conda is activated:

# conda config --set auto_activate_base false
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/neil/miniforge3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/neil/miniforge3/etc/profile.d/conda.sh" ]; then
        . "/home/neil/miniforge3/etc/profile.d/conda.sh"
    else
        export PATH="/home/neil/miniforge3/bin:$PATH"
    fi
fi
unset __conda_setup

if [ -f "/home/neil/miniforge3/etc/profile.d/mamba.sh" ]; then
    . "/home/neil/miniforge3/etc/profile.d/mamba.sh"
fi
# <<< conda initialize <<<

# pnpm
export PNPM_HOME="/home/neil/.local/share/pnpm"
case ":$PATH:" in
  *":$PNPM_HOME:"*) ;;
  *) export PATH="$PNPM_HOME:$PATH" ;;
esac
# pnpm end
