# Put your fun stuff here.
alias du1='du -h --max-depth=1 "$@"| sort -h'
alias grep='grep -n --colour=auto'
alias egrep='grep -E -n --colour=auto'
alias fgrep='fgrep -n --colour=auto'
alias diff='colordiff'
alias tree='tree -fhD'
alias bc='bc -l'
alias ip='ip -c'
alias restart_emacs='sudo rc-service emacs.neil restart'
alias ymd='date +%Y%m%d'

## Grip alias
alias rip_done='killall grip && eject /dev/sr0'

## Stata-MP alias
alias xstata='LD_LIBRARY_PATH=/usr/local/stata/lib /usr/local/stata/xstata-mp'
alias stata='LD_LIBRARY_PATH=/usr/local/stata/lib /usr/local/stata/stata-mp'

## Alias' for restarting
alias shutup='su -c "shutdown -h now"'
alias restart='su -c "reboot"'
alias sleep='su -c "hibernate-ram"'

## Misc
alias temperature='watch -n1 sensors'

## Print stage in colour
function print_stage() {
    echo -e " \e[33m*\e[39m \e[94m$1\e[39m"
}


## Crypto
rate () {
    curl rate.sx/"$@"
}

## JOSM
josm () {
    java -jar ~/.local/bin/josm-tested.jar
}

## grep history
greph() {
    history | grep "$@"
}

## Bash Automated Testing System
bats() {
  BATS_RUN_SKIPPED=true command bats *.bats
}
#
# Restart Emacs daemon conditional on system
if [ "${HOST}" = "kimura" ] ; then
    alias emacsr='sudo rc-service emacs.neil restart'
  else
    alias emacsr='systemctl restart --user emacs.servic'e
fi

## Wake On Lan
alias wol_kimura='wol 38:d5:47:e0:16:1f'
alias wol_alarmpi_4b='wol dc:a6:32:c4:71:b9'

## Gentoo
alias genlopt='genlop -l --date today'
alias genlopw='genlop -l --date week'

## Arch
alias systemctl.emacs='systemctl --user restart emacs'
alias pacman.last='expac --timefmt="%Y-%m-%d %T" "%l\t%n" | sort | tail -n 100'
# Git

## SSH alias'
alias slacker='ssh root@slacker'
alias crow='ssh crow'
alias darwin='ssh darwin'
alias fisher='ssh fisher'
alias hamilton='ssh hamilton'
alias haldane='ssh haldane'
alias kimura='ssh kimura'
alias kimura.local='ssh 192.168.1.45'
alias kimura.fisher='ssh -t kimura "ssh fisher"'
alias openwrt='ssh openwrt'
alias ovh='ssh ovh'
alias morgan='ssh morgan'
alias work='ssh work'
alias mendel='ssh mendel'
alias laptop='ssh laptop'
alias alarmpi='ssh alarmpi'
alias alarmpi_ext='ssh neil@alarmpi.net'
alias alarmpi2='ssh neil@alarmpi2'
alias alarmpi3='ssh neil@alarmpi3'
alias alarmpi4='ssh neil@alarmpi4'
alias alarmpi5='ssh neil@alarmpi5'
alias alarmpi6='ssh neil@alarmpi6'
alias alarmpi7='ssh neil@alarmpi7'
alias alarmpi-4b='ssh neil@alarmpi-4b'
alias jim='ssh nshephard@frankexchangeofviews.co.uk'
alias curbar='ssh sa_ac1nds@curbar'

## tmux aliases
alias tmux_ssh='tmux attach -t ssh'

## OVH VPS

## TMB servers
alias tmb_dave='ssh tmb_dave'
alias tmb_emily='ssh tmb_emily'
alias tmb_sara='ssh slackline@sara.themixingbowl.org'

# Alias for commonly used directories (@ home)

alias code_clinic='cd ~/work/code_clinic/'
alias emacsd='cd ~/.config/emacs/'
alias hub='cd ~/work/git/hub'
alias lab='cd ~/work/git/lab/nshephard'
alias forgejo='cd ~/work/git/forgejo/nshephard'
alias nsrse='cd ~/work/git/hub/ns-rse'
alias afm='cd ~/work/git/hub/AFM-SPM'
alias org='cd ~/org'
alias orgroam='cd ~/org-roam'
alias pics='cd ~/pics'
alias projects='cd ~/work/projects/'
alias templates='cd ~/work/templates'

# rsync aliases
alias rsync='rsync -a --info=progress2'
alias rsync_html='rsync ~/org/*.html ~/org/export ~/org/training ovh:~/www/. --exclude="*.org"'

# Broad Git alias https://mitxela.com/projects/dotfiles_management
alias dotfiles='git --git-dir=${HOME}/dotfiles --work-tree=/'

# Python
alias pl='pylint --rcfile=~/.pylintrc'
alias pt='pytest --pylint --mpl --cov-report=html'
alias pip_upgrade="pip list --outdated --format=json | jq -r '.[] | .name'  | xargs -n1 pip install -U"
alias pipu="pip list --outdated --format=json | jq -r '.[] | .name'  | xargs -n1 pip install -U"

# Docker


# Equery dependencies
alias eqf='equery f'
alias equ='equery u'
alias eqh='equery h'
alias eqa='equery a'
alias eqb='equery b'
alias eql='equery l'
alias eqd='equery d'
alias eqg='equery g'
alias eqc='equery c'
alias eqk='equery k'
alias eqm='equery m'
alias eqy='equery y'
alias eqs='equery s'
alias eqw='equery w'

# Emacs
alias emacsc='${EMACSCLIENT} -nc --socket-name=${EMACS_SOCKET} &'
alias emacsn='${EMACSCLIENT} -nw --socket-name=${EMACS_SOCKET}'

# alias emacsc='/usr/bin/emacsclient -nc --socket-name=/tmp/emacs1000/server'
# alias emacsn='/usr/bin/emacsclient -nw --socket-name=/tmp/emacs1000/server'

# Create a new directory and enter it
mkcd() {
    mkdir -p "$@"
    cd "$_" # || exit
}

# Generate QR codes https://www.linuxjournal.com/content/boost-productivity-bash-tips-and-tricks
qrgen () {
    printf "$@" | curl -F-=\<- qrenco.de
}


# Temporary aliases if the exist
if [ -f "~/.bash_aliases_tmp" ]; then
    source ~/.bash_aliases_tmp
fi

# Work Alias
alias vm_ubuntu22='qemu-system-x86_64 -cdrom ~/vm/iso/ubuntu-22.04.1-desktop-amd64.iso -boot order=d -drive file=${HOME}/vm/disk/ubuntu22,format=raw -m 4G -enable-kvm'
alias vm_xubuntu22='qemu-system-x86_64 -cdrom ~/vm/iso/xubuntu-22.04.2-desktop-amd64.iso -boot order=d -drive file=${HOME}/vm/disk/xubuntu22,format=raw -m 4G -enable-kvm'
alias vm_win10='qemu-system-x86_64 -cdrom ~/vm/iso/SW_DVD9_Win_Pro_10_22H2.1_64BIT_English_Pro_Ent_EDU_N_MLF_X23-31722.ISO -boot order=d -drive file=${HOME}/vm/disk/win10.img,format=qcow2 -m 4G -enable-kvm -vga std -cpu host -net nic,model=e1000 -net user -device qemu-xhci'
# Central VMs
alias curbar='ssh sa_ac1nds@curbar'

# Arch
KERNVERS="$(uname -r)"
alias kernel_check='pacman -Q linux | grep -vq "${KERNVERS/-/.}" && echo "Kernel has been upgraded, please reboot."'

# Audio and Video
alias sc-dlp='yt-dlp --write-thumbnail --embed-thumbnail -x'
alias yt-dlp='yt-dlp --write-thumbnail --embed-thumbnail'
alias mp4mp3='ffmpeg -b:a320K'

# Fittracke
alias sudo_fittrackee='sudo su fittrackee'
