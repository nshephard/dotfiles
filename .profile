export CUPS_USER='ac1nds'
. "$HOME/.cargo/env"
export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_CACHE_HOME="${HOME}/.cache"
# The following will cause ZSH to look for .zshrc in the given directory
# export ZDOTDIR="${XDG_CONFIG_HOME}/zsh"
