export XDG_CONFIG_HOME="${HOME}/.config/"
export XDG_DATA_HOME="${HOME}/.local/share/"
if [ -z "${XDG_RUNTIME_DIR}" ]; then
     export XDG_RUNTIME_DIR="/run/user/${UID}/"
     if [ ! -d "${XDG_RUNTIME_DIR}" ]; then
         mkdir "${XDG_RUNTIME_DIR}"
         chmod 0700 "${XDG_RUNTIME_DIR}"
     fi
fi
