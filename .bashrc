# /etc/skel/.bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !


# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi

# Conditionally colour my command prompt
# Set the terminal and title
# case $TERM in
#     (xterm*) set_title='\[\e]0;\u@\h: \w\a\]';;
#     (*) set_title=
# esac
export PS1=$set_title'\[\033[01;32m\]\u@\h\[\033[01;34m\] \w \$\[\033[00m\] '
## Gentoo Default
#export PS1='\[\033[01;32m\]\u@\h\[\033[01;34m\] \w \$\[\033[00m\] '
## PS1 Generated http://bashrcgenerator.com/
## For more on colours see http://tldp.org/HOWTO/Bash-Prompt-HOWTO/x329.html
# Green
#export PS1="\[\033[38;5;10m\]\u@\h\[$(tput sgr0)\]\[\033[38;5;4m\]\w \\$\[$(tput sgr0)\]"
# Yellow
#export PS1="\[\033[38;5;11m\]\u@\h\[$(tput sgr0)\]\[\033[38;5;4m\]\w \\$\[$(tput sgr0)\]"
# Purple
#export PS1="\[\033[38;5;13m\]\u@\h\[$(tput sgr0)\]\[\033[38;5;4m\]\w \\$\[$(tput sgr0)\]"
# Cyan
#export PS1="\[\033[38;5;14m\]\u@\h\[$(tput sgr0)\]\[\033[38;5;4m\]\w \\$\[$(tput sgr0)\]"
# White
#export PS1="\u@\h\[$(tput sgr0)\]\[\033[38;5;4m\]\w \\$\[$(tput sgr0)\]"

# Window Title (adapted for Emacs, see https://stackoverflow.com/a/39496135/1444043)
if [ -z "$INSIDE_EMACS" ];
   then
       export PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD}\007"'
else
    export PROMPT_COMMAND=''
fi

# Additions to system PATH
#PATH="/home/neil/bin:$PATH:/usr/local/stata/:/usr/local/stattransfer/:.:~/share/idea-IC-181.4668.68/bin"

# Python
export PYTHONPATH=".:${PYTHONPATH}:"

## NPM stuff
PATH="$HOME/.node/bin:$HOME/.cargo/bin:$HOME/.local/bin:$HOME/bin:$PATH"
NODE_PATH="$HOME/.node/lib/node_modules:$NODE_PATH"
export PATH
export IPLAYER_OUTDIR="~/video/iplayer/"

# Make history smart
HISTFILESIZE=100000
HISTSIZE=6000
export HISTCONTROL=erasedups

## Source aliases file (if it exists)
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi


## Functions
mkcd() {
    mkdir "$@" || return
    shift $(( $# - 1 ))
    cd "$1"
}

# Keychain
KEYCHAIN=$(which keychain)
if [ "${HOST}" = "kimura" ] || [ "${HOST}" = "haldane" ] || [ "${HOST}" = "mendel" ] || [ "${HOST}" = "crow" ] || [ "${HOSTNAME}" = "kimura" ] || [ "${HOSTNAME}" = "haldane" ] || [ "${HOSTNAME}" = "mendel" ] || [ "${HOSTNAME}" = "crow" ]  ; then
    # export SSH_AUTH_SOCK='$(gpgconf --list-dirs agent-ssh-socket)'
    ${KEYCHAIN} --agents ssh ~/.ssh/id_ed25519
    ${KEYCHAIN} --agents ssh ~/.ssh/haldane_ed25519
    if [ "${HOST}" = "crow" ]  ; then
        ${KEYCHAIN} --agents ssh ~/.ssh/crow_ed25519
    fi
    . ~/.keychain/${HOST}-sh
    ${KEYCHAIN} --agents gpg nshephard@protonmail.com
    ${KEYCHAIN} --agents gpg n.shephard@sheffield.ac.uk
    . ~/.keychain/${HOST}-sh-gpg
    if [[ -n "$SSH_CONNECTION" ]] ;then
        export GPG_TTY=$(tty)
    	# export PINENTRY_USER_DATA="USE_CURSES=1"
	    export PINENTRY_USER_DATA="USE_TTY=1"
    fi
fi

# SciPy image viewer
export SCIPY_PIL_IMAGE_VIEWER=ristretto


# If we've SSH'd in connect/start a tmux session
if [[ -z "$TMUX" ]] && [ "$SSH_TTY" != "" ]; then
    tmux attach-session -t ssh_tmux || tmux new-session -s ssh_tmux
fi

# Python Virtual Environments
export WORKON_HOME="$HOME/.virtualenvs"
if [ "$HOST" = "kimura" ] || [ "$HOSTNAME" = "kimura" ] ||  [ "$HOST" = "haldane" ] || [ "$HOSTNAME" = "haldane" ] ||  [ "$HOST" = "crow" ] || [ "$HOSTNAME" = "crow" ] ; then
  export PROJECT_HOME="$HOME/work/python"
  VENV_WRAPPER=$(which virtualenvwrapper.sh)
    source ${VENV_WRAPPER}
else
    source /home/neil.shephard/.local/bin/virtualenvwrapper.sh
fi

# vterm customisation for Emacs (https://github.com/akermu/emacs-libvterm)
vterm_printf(){
    if [ -n "$TMUX" ]; then
        # Tell tmux to pass the escape sequences through
        # (Source: http://permalink.gmane.org/gmane.comp.terminal-emulators.tmux.user/1324)
        printf "\ePtmux;\e\e]%s\007\e\\" "$1"
    elif [ "${TERM%%-*}" = "screen" ]; then
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$1"
    else
        printf "\e]%s\e\\" "$1"
    fi
}
if [[ "$INSIDE_EMACS" = 'vterm' ]]; then
    function clear(){
        vterm_printf "51;Evterm-clear-scrollback";
        tput clear;
    }
fi
if [[ "$INSIDE_EMACS" = 'vterm' ]]; then
    alias clear='vterm_printf "51;Evterm-clear-scrollback";tput clear'
fi
vterm_prompt_end(){
    vterm_printf "51;A$(whoami)@${HOSTNAME}:$(pwd)"
}
PS1=$PS1'\[$(vterm_prompt_end)\]'

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/neil/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/neil/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/neil/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/neil/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
. "$HOME/.cargo/env"
