{ config, lib, pkgs, ... }:

{
  home-manager = {
    username = "neil";
    homeDirectory = "/home/neil";
  };

  # link the configuration file in current directory to the specified location in home directory
  # home.file.".config/i3/wallpaper.jpg".source = ./wallpaper.jpg;

  # link all files in `./scripts` to `~/.config/i3/scripts`
  # home.file.".config/i3/scripts" = {
  #   source = ./scripts;
  #   recursive = true;   # link recursively
  #   executable = true;  # make all files executable
  # };

  # encode the file content in nix configuration file directly
  # home.file.".xxx".text = ''
  #     xxx
  # '';

  # set cursor size and dpi for 4k monitor
  # xresources.properties = {
  #   "Xcursor.size" = 16;
  #   "Xft.dpi" = 172;
  # };

  # Set authorized SSH keys in file
  # home.file.".ssh/authorized_keys".text = ''
  #   ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHxSYDkDPPggAhAfPYKSg259M5niw+ewj4LxMF/wxTiJ neil@kimura
  #   ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIARcKip5cMGhQvOooF0sAFTvLAj8R7wz+7cPV9BndPtL neil@haldane
  # '';

  # Packages that should be installed to the user profile.
  home.packages = with pkgs; [
    # here is some command line tools I use frequently
    # feel free to add your own or remove some of them

    # neofetch
    # nnn # terminal file manager

    # archives
    bzip2
    gzip
    p7zip
    unzip
    xz
    zip

    # utils
    fzf # A command-line fuzzy finder
    most # Useful parser
    ripgrep # recursively searches directories for a regex pattern
    yq-go # yaml processor https://github.com/mikefarah/yq

    # networking tools
    mtr # A network diagnostic tool
    iperf3
    dnsutils  # `dig` + `nslookup`
    ldns # replacement of `dig`, it provide the command `drill`
    aria2 # A lightweight multi-protocol & multi-source command-line download utility
    socat # replacement of openbsd-netcat
    nmap # A utility for network discovery and security auditing
    ipcalc  # it is a calculator for the IPv4/v6 addresses

   # misc
   cowsay
   file
   gawk
   ghostty
   gnupg
   gnused
   gnused
   gnutar
   stow
   tree
   which
   zstd

   # nix related
   #
   # it provides the command `nom` works just like `nix`
   # with more details log output
   nix-output-monitor

   # productivity
   glow # markdown previewer in terminal
   hugo # static site generator

   # graphics
   darktable
   evince
   gimp
   hugin

   # statistics
   julia
   R

   # python (see also homemanager/python.nix)
   ruff
   uv

   # multimedia
   asunder
   audacious
   audacity
   calibre
   vlc
   yt-dlp
   # monitoring
   btop  # replacement of htop/nmon
   iotop # io monitoring
   iftop # network monitoring

   # system call monitoring
   lsof # list open files
   ltrace # library call monitoring
   strace # system call monitoring

   # shell
   direnv
   oh-my-zsh
   zsh-autopair
   zsh-completions
   zsh-f-sy-h
   zsh-nix-shell
   zsh-powerlevel10k

   # android
   android-tools

   # developer tools
   ltex-ls

   # system tools
   ethtool
   lm_sensors # for `sensors` command
   lshw
   pciutils # lspci
   sysstat
   usbutils # lsusb

 ];

  # basic configuration of git, please change to your own
  programs = {

    bash = {
      enable = false;
      enableCompletion = true;
      # add your custom bashrc here
      # bashrcExtra = ''
      #   export PATH="$PATH:$HOME/bin:$HOME/.local/bin:$HOME/go/bin"
      # '';
      # # set some aliases, feel free to add more or remove some
      # shellAliases = {
      #   k = "kubectl";
      #   urldecode = "python3 -c 'import sys, urllib.parse as ul; print(ul.unquote_plus(sys.stdin.read()))'";
      #   urlencode = "python3 -c 'import sys, urllib.parse as ul; print(ul.quote_plus(sys.stdin.read()))'";
      # };
    }; # end bash

    browserpass = {
      enable = true;
    }; # end browserpass

    # https://github.com/nix-community/nix-direnv
    direnv = {
      enable = true;
      enableBashIntegration = true;
      enableZshIntegration = true;
      nix-direnv.enable = true;
    }; # end direnv

    emacs = {
      enable = true;
    }; # end emacs

    # ghosttty = {
    #   enable = true;
    # }; # end ghosttty

    git = {
      enable = true;
      # userName = "Neil Shephard";
      # userEmail = "nshephard@protonmail.com";
    }; # end git

    kitty = {
      enable = true;
      # settings = {
      #   background_opacity = "0.7";
      #   dynamic_background_opacity = true;
      #   enable_audio_bell = false;
      #   tab_bar_edge = "top";
      # };
    }; # end kitty

    tmux = {
      enable = true;
    }; # end tmux

    zsh = {
      enable = true;
      #enableCompletion = true;
      #autosuggestions.enable = true;
      #syntaxHighlighting.enable = true;
      #history = {
      #  save = 1000000;
      #  ignoreAllDups = true;
      #  ignoreSpace = true;
      #};
      #shellAliases = {
      #  shutup = "sudo -c shutdown -h now";
      #  update = "sudo nixos-rebuild switch";
      #};
      oh-my-zsh = {
        enable = true;
        # plugins = [
        #  "git"
        #  "pass"
        #  "pip"
        #  "rsync"
        #  "common-aliases"
        #];
        theme = "";
      };
    }; # end zsh
  }; # end programs
  # Link dotfiles (would be nice to use stow to do this!)
  xdg.configFile = {
    "atuin/config.toml".source = ../../../home/neil/dotfiles/.config/atuin/config.toml;
    "bat/conf".source = ../../../home/neil/dotfiles/.config/bat/conf;
    "beets/config.yaml".source = ../../../home/neil/dotfiles/.config/beets/config.yaml;
    "browsh/config.toml".source = ../../../home/neil/dotfiles/.config/browsh/config.toml;
    "easytag/easytagrc".source = ../../../home/neil/dotfiles/.config/easytag/easytagrc;
    "gedit/accels".source = ../../../home/neil/dotfiles/.config/gedit/accels;
    "gh/config.yml".source = ../../../home/neil/dotfiles/.config/gh/config.yml;
    "gh/hosts.yml".source = ../../../home/neil/dotfiles/.config/gh/hosts.yml;
    "git/config".source = ../../../home/neil/dotfiles/.config/git/config;
    "hatch/config.toml".source = ../../../home/neil/dotfiles/.config/hatch/config.toml;
    "home-manager/home.nix".source = ../../../home/neil/dotfiles/.config/home-manager/home.nix;
    "htop/htoprc".source = ../../../home/neil/dotfiles/.config/htop/htoprc;
    "jj/conf".source = ../../../home/neil/dotfiles/.config/jj/config.toml;
    "kitty/kitty.conf".source = ../../../home/neil/dotfiles/.config/kitty/kitty.conf;
    "lsd/conf".source = ../../../home/neil/dotfiles/.config/lsd/conf;
    "mongo/mongod.conf".source = ../../../home/neil/dotfiles/.config/mongo/mongod.conf;
    "nano/.nanorc".source = ../../../home/neil/dotfiles/.config/nano/.nanorc;
    "nyxt/config.lisp".source = ../../../home/neil/dotfiles/.config/nyxt/config.lisp;
    "picom/picom.conf".source = ../../../home/neil/dotfiles/.config/picom/picom.conf;
    "podget/podgetrc".source = ../../../home/neil/dotfiles/.config/podget/podgetrc;
    "podget/serverlist".source = ../../../home/neil/dotfiles/.config/podget/serverlist;
    ".pycodestyle".source = ../../../home/neil/dotfiles/.config/.pycodestyle;
    "rockbox.org/RockboxUtility.ini".source = ../../../home/neil/dotfiles/.config/rockbox.org/RockboxUtility.ini;
    "thefuck/settings.py".source = ../../../home/neil/dotfiles/.config/thefuck/settings.py;
    "tmux/tmux.conf".source = ../../../home/neil/dotfiles/.config/tmux/tmux.conf;
    "vlc/vlc-qt-interface.conf".source = ../../../home/neil/dotfiles/.config/vlc/vlc-qt-interface.conf;
    "vlc/vlcrc".source = ../../../home/neil/dotfiles/.config/vlc/vlcrc;
    "yapf/style".source = ../../../home/neil/dotfiles/.config/yapf/style;
    "yt-dlp/config".source = ../../../home/neil/dotfiles/.config/yt-dlp/config;
    # "".source = ./dotfiles/
  }; # end xdg
  home.file = {
    ".authinfo.gpg".source = ../../../home/neil/dotfiles/.authinfo.gpg;
    ".bash_aliases".source = ../../../home/neil/dotfiles/.bash_aliases;
    ".bash_logout".source = ../../../home/neil/dotfiles/.bash_logout;
    ".bash_profile".source = lib.mkForce ../../../home/neil/dotfiles/.bash_profile;
    ".bashrc".source = lib.mkForce ../../../home/neil/dotfiles/.bashrc;
    ".p10k.zsh".source = ../../../home/neil/dotfiles/.p10k.zsh;
    ".zprofile".source = ../../../home/neil/dotfiles/.zprofile;
    ".zshrc".source = lib.mkForce ../../../home/neil/dotfiles/.zshrc;
    ".zshrc.zni".source = ../../../home/neil/dotfiles/.zshrc.zni;
  }; # end home

  # This value determines the home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update home Manager without changing this value. See
  # the home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "24.11";

  # Let home Manager install and manage itself.
  programs.home-manager.enable = true;
}
