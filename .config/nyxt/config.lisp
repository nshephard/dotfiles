;;; Config for Nyxt browser

;;; Emacs keybindings
(define-configuration buffer
  ((default-modes
    (pushnew 'nyxt/mode/emacs:emacs-mode %slot-value%))))

;;; Search Engines nyxt:manual#search-engines
(defvar *my-search-engines*
  (list
   '("python3" "https://docs.python.org/3/search.html?q=~a"
     "https://docs.python.org/3")
   '("doi" "https://dx.doi.org/~a" "https://dx.doi.org/")
   '("duckduckgo" "https://duckduckgo.com/search?q=~a" "https://duckduckgo.com"))
  "List of search engines.")

(define-configuration context-buffer
  "Go through the search engines above and `make-search-engine' out of them."
  ((search-engines
    (append
     (mapcar (lambda (engine) (apply 'make-search-engine engine))
             *my-search-engines*)
     %slot-default%))))
