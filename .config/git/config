# Useful resources for config options...
#
# https://jvns.ca/blog/2024/02/16/popular-git-config-options/
# https://blog.gitbutler.com/how-git-core-devs-configure-git/
[user]
	email = nshephard@protonmail.com
	name = Neil Shephard
[column]
	ui = auto
[commit]
#	gpgsign = true
[core]
	editor = nano
	sshCommand = ssh -i ~/.ssh/id_ed25519 -F /dev/null
	attributesFile = $HOME/.gitattributes
	autocrlf = input
	excludesfile = ~/.config/git/.gitignore
[branch]
	sort = -committerdate
[color]
	ui = auto
[credential]
	helper = cache
[github]
	user = ns-rse
	email = nshephard@protonmail.com
[gitlab]
	user = nshephard
	email = nshephard@protonmail.com
[help]
	autocorrect = prompt
[init]
	defaultBranch = main
[log]
	date = iso
[merge]
	conflictStyle = diff3
[pack]
	window = 0
	windowMemory = 100m
	packSizeLimit = 100m
	threads = 1
[pull]
	# https://megakemp.com/2019/03/20/the-case-for-pull-rebase/
	rebase = true
[push]
	default = current
	autoSetupRemote = true
[rebase]
	autostash = true
	autosquash = true
	updateRefs = true
[rerere]
	enabled = true
	autoupdate = true
[remote]
    pushDefault = origin
[tag]
	sort = -committerdate
	gpgsign = true
[diff]
	external = difft
	tool = difftastic
	word-diff = color
	# https://sergiswriting.com/better-colors-for-moved-text-in-git/
	color-moved = zebra
	color-moved-ws = allow-indentation-change
[difftool]
	prompt = false
[difftool "difftastic"]
	cmd = difft "$LOCAL" "$REMOTE"
[diff "common-lisp"]
	xfuncname="^\\((def\\S+\\s+\\S+)"
[diff "elisp"]
	xfuncname="^\\((((def\\S+)|use-package)\\s+\\S+)"
[diff "org"]
	xfunc="^\\*__(.*)$"
[alias]
	dft = difftool
	# lg1 = "log --graph --abbrev-commit --decorate --pretty='\\''%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset'\\"'
	# https://mastodon.social/@MichalBryxi@mastodon.coffee/109808133745642868
	# git_log_date_merged = 'git log --merges --after="2023-01-03" --before="2023-02-05" --pretty=format:"%h %an %ad" --first-parent master'
	# https://stackoverflow.com/a/2444495/1444043
	# pub = "!f() { git push -u ${1:-origin} `git symbolic-ref HEAD`; }; f"
	df = diff --name-only
	# Checkout merge requests by head ref
	# https://docs.gitlab.com/ee/user/project/merge_requests/reviews/index.html#checkout-merge-requests-locally-through-the-head-ref
	mr = !sh -c 'git fetch $1 merge-requests/$2/head:mr-$1-$2 && git checkout mr-$1-$2' -
	unstage = reset HEAD --
	fpush = push --force
    logp = log --pretty=format:"%C(yellow)%h\\ %C(green)%ad%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate --date=short --graph
	undo = reset HEAD~
	branchdate = branch --sort=-committerdate
	patch = diff --no-ext-diff
# Need everything twice because of symlinks it seems
[includeIf "gitdir:~/work/teaching/"]
    path = ~/.config/git/config_work
[includeIf "gitdir:~/work/teaching/zero-hero-2024-06"]
    path = ~/.config/git/config_work
[includeIf "gitdir:~/work/git/hub/ns-rse/"]
    path = ~/.config/git/config_work
[includeIf "gitdir:/mnt/work/git/hub/ns-rse/"]
    path = ~/.config/git/config_work
[includeIf "gitdir:~/work/git/hub/AFM-SPM/"]
    path = ~/.config/git/config_work
[includeIf "gitdir:/mnt/work/git/hub/AFM-SPM/"]
    path = ~/.config/git/config_work
[includeIf "gitdir:~/work/git/hub/jjriley1/"]
    path = ~/.config/git/config_work
[includeIf "gitdir:~/work/git/hub/SheffieldR/"]
    path = ~/.config/git/config_work
[includeIf "gitdir:/mnt/work/git/hub/SheffieldR/"]
    path = ~/.config/git/config_work
[includeIf "gitdir:~/work/git/hub/RSE-Sheffield/"]
    path = ~/.config/git/config_work
[includeIf "gitdir:/mnt/work/git/hub/RSE-Sheffield/"]
    path = ~/.config/git/config_work
[includeIf "gitdir:~/work/git/hub/sudlab/"]
    path = ~/.config/git/config_work
[includeIf "gitdir:/mnt/work/git/hub/sudlab/"]
    path = ~/.config/git/config_work
[includeIf "gitdir:/mnt/work/git/hub/mdp21oe/"]
    path = ~/.config/git/config_work
[includeIf "gitdir:/mnt/work/git/hub/RSEToolkit/"]
	path = ~/.config/git/config_work
[includeIf "gitdir:~/work/git/hub/jjriley1"]
	path = ~/.config/git/config_work
[includeIf "gitdir:~/work/git/hub/socrse"]
	path = ~/.config/git/config_work
[includeIf "gitdir:/mnt/work/git/hub/socrse"]
	path = ~/.config/git/config_work
[includeIf "gitdir:/mnt/work/git/hub/FAIR2-for-research-software"]
	path = ~/.config/git/config_work
[includeIf "gitdir:~/work/git/hub/Mesnage-Org/glam"]
	path = ~/.config/git/config_work
[includeIf "gitdir:~/work/git/hub/Mesnage-Org/pgfinder"]
	path = ~/.config/git/config_work
[includeIf "gitdir:~/work/git/hub/slackline/"]
    path = ~/.config/git/config_personal
[includeIf "gitdir:/mnt/work/git/hub/slackline/"]
    path = ~/.config/git/config_personal
[includeIf "gitdir:~/work/git/hub/ebrithiljonas/fittrackee-uploader"]
    path = ~/.config/git/config_personal
[includeIf "gitdir:/mnt/work/git/hub/ebrithiljonas/fittrackee-uploader"]
    path = ~/.config/git/config_personal
[includeIf "gitdir:~/work/git/lab/"]
    path = ~/.config/git/config_personal
[includeIf "gitdir:/mnt/work/git/lab/"]
    path = ~/.config/git/config_personal
[includeIf "gitdir:/mnt/work/git/gentoo.org/"]
    path = ~/.config/git/config_personal
[includeIf "gitdir:~/work/git/gentoo.org/"]
    path = ~/.config/git/config_personal
[includeIf "gitdir:~/org/"]
    path = ~/.config/git/config_personal
[includeIf "gitdir:/home/neil/org/"]
    path = ~/.config/git/config_personal
[url "git@github.com:"]
	insteadof = https://github.com/
[url "git@gitlab.com:"]
	insteadof = https://gitlab.com/
[url "git@forgejo.nshephard.dev"]
	insteadof = https://forgejo.nshephard.dev/
[maintenance]
	repo = /home/neil/org
	repo = /home/neil/.config/emacs
	repo = /home/neil/org-roam
